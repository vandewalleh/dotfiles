set shell=/bin/bash

set autoindent
set encoding=utf-8
set number
set relativenumber
set timeoutlen=1000 ttimeoutlen=0

vnoremap < <gv
vnoremap > >gv

set expandtab
set shiftwidth=4
set smarttab
set softtabstop=0
set tabstop=8
set updatetime=100
set incsearch
set scrolloff=2
set noshowmode

call plug#begin()

"syntax
Plug 'aklt/plantuml-syntax', { 'for': 'plantuml' }
Plug 'dag/vim-fish', { 'for': 'fish' }
"Plug 'fatih/vim-go', { 'for': 'go' }
Plug 'leafgarland/typescript-vim', { 'for': 'typescript' }
"Plug 'udalov/kotlin-vim', { 'for': 'kotlin' }
Plug 'vim-pandoc/vim-pandoc-syntax', { 'for': 'markdown.pandoc' }
Plug 'vim-pandoc/vim-pandoc', { 'for': 'markdown.pandoc' }
Plug 'kovetskiy/sxhkd-vim'
Plug 'zah/nim.vim'
Plug 'cespare/vim-toml'
Plug 'ElmCast/elm-vim'
Plug 'vito-c/jq.vim'

"fzf
Plug '/usr/bin/fzf'
Plug 'junegunn/fzf.vim'

"status line
Plug 'itchyny/lightline.vim'

"themes
Plug 'connorholyday/vim-snazzy'
Plug 'nerdypepper/agila.vim'
"Plug 'morhetz/gruvbox'

"others
Plug 'airblade/vim-gitgutter'
Plug 'ap/vim-css-color'
Plug 'tpope/vim-surround'
Plug 'Valloric/YouCompleteMe'
Plug 'w0rp/ale'

call plug#end()

augroup pandoc_syntax
    au! BufNewFile,BufFilePre,BufRead *.md set filetype=markdown.pandoc
augroup END

let g:ale_c_clang_options='-Wall -Wextra -lpthread -lrt -std=gnu99 -lncurses'
let g:ale_c_gcc_options='-Wall -Wextra -lpthread -lrt -std=gnu99 -lncurses'

let g:ale_completion_enabled=0
let g:ale_typescript_prettier_options='-t es2018'

let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'c': ['clang-format'],
\   'javascript': ['prettier'],
\   'css': ['prettier'],
\    'elm': ['elm-format'],
\}

set laststatus=2
let g:lightline = {
      \ 'colorscheme': 'snazzy',
      \ }

let g:SnazzyTransparent = 1
colorscheme snazzy

let g:pandoc#syntax#conceal#use=0
let g:pandoc#syntax#style#use_definition_lists=0
let g:pandoc#modules#disabled = ["folding", "completion"]
let g:pandoc#toc#close_after_navigating=0
let g:pandoc#spell#enabled=0
let g:pandoc#toc#position="right"
let g:pandoc#syntax#codeblocks#embeds#langs = ["plantuml"]

let g:gitgutter_grep = 'rg'


"remove trailing whitespace
nnoremap <silent> <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>:retab<CR>

let mapleader = " "
let maplocalleader = " "

nnoremap <silent> <leader>f :ALEFix<CR>
nnoremap <silent> <leader><Space> :Files<CR>
nnoremap <silent> <leader>m :make<CR><CR>
nnoremap <silent> <leader>t :TOC<CR>
nnoremap <silent> <leader>t :!pandoc --atx-headers --columns 100 % -o % -t markdown<CR>

let $FZF_DEFAULT_COMMAND = 'fd -Htf -E.git'

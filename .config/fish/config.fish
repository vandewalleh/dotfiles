set -gx PAGER 'less'
set -gx LESS '-F -g -i -M -R -S -X -z-4'
set -gx VISUAL 'vim'
set -gx EDITOR 'vim'
set -gx TERMINAL 'kitty'
set -gx BROWSER 'firefox'
set -gx XDG_CONFIG_HOME '/home/hubert/.config'
set -gx _JAVA_AWT_WM_NONREPARENTING '1'
set -gx FZF_DEFAULT_COMMAND 'fd -tf'
set -gx FZF_DEFAULT_OPTS '--height 35% --color pointer:222,bg+:-1'
set -gx LS_COLORS (vivid generate snazzy)

if status is-login
    if test (tty) = /dev/tty1
        exec startx
    end
end

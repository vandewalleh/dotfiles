function audio
    set -l sinks (pactl list short sinks | awk '{print $2}')
    if test "$argv" = -l
        pactl list short sinks | awk '{print $2}'
    else
        set -l sink (pactl list short sinks | awk '{print $2}' | fzf) && pactl set-default-sink $sink
    end
end

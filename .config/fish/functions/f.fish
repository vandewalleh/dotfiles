function f
    set -l path (fd -H -td '^\.git$' ~/Workspace -x echo '{//}' | string replace "$HOME/Workspace/" ''| fzf)
    cd "$HOME/Workspace/$path"
end

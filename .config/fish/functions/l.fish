function l --wraps exa --description 'alias l ls -1'
    exa -1 $argv
end

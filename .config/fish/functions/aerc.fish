function aerc --description 'alias aerc env TERM=xterm-256color aerc'
    env TERM=xterm-256color aerc $argv
end

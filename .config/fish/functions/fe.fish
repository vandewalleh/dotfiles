function fe --description 'fuzzy edit'
    set -l file (fd -tf -c always | fzf --ansi) && $EDITOR $file
end

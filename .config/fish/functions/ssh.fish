function ssh --wraps ssh --description 'ssh with correct colors'
    command env TERM=xterm-256color ssh $argv
end

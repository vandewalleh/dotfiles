function vps
    set -l dest (echo vpsmel\nvpsgui\nvpshub | fzf --height 6) && ssh $dest $argv
end

function tree --wraps exa --description 'alias tree exa -T'
    exa -T $argv
end

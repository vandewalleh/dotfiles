function ncdu --description 'alias ncdu ncdu -x --color=dark'
    command ncdu -x --color=dark $argv
end

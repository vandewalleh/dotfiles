function mdtopdf
    set -l input $argv[1]

    if test (count $argv) -ne 1
        echo (set_color red)"missing file"
        return 1
    end

    if not test -f $input
        echo (set_color red)"not a file"
        return 1
    end

    if not string match -a -q -r '.+\.md$' $input
        echo (set_color red)'not a markdown file'
        return 1
    end

    set -l output (echo (basename $input .md).pdf)
    echo -- (set_color blue)$input (set_color green)'->' (set_color blue)$output
    pandoc $input -o $output --from markdown --template eisvogel.tex --pdf-engine=xelatex
end

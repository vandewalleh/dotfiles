function css --description 'copy signal custom css to clipboard'
    cat ~/Workspace/signal-custom/custom.css | xclip -selection clipboard
end

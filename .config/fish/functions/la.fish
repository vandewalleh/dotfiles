function la --wraps exa
    exa -lah --git $argv
end

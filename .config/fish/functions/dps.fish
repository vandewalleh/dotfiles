function dps
    set -l green (set_color -o green)
    set -l blue (set_color -o blue)
    set -l normal (set_color normal)

    docker ps --format (echo "$green{{.Names}}$normal
    \n$blue\tContainer ID: $normal{{.ID}}
    \n$blue\tCommand: $normal{{.Command}}
    \n$blue\tImage: $normal{{.Image}}
    \n$blue\tCreatedAt: $normal{{.CreatedAt}}
    \n$blue\tStatus: $normal{{.Status}}
    \n$blue\tPorts: $normal{{.Ports}}" | string join '') $argv
end

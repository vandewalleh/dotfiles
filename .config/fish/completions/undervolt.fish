complete -c undervolt -s h -l help    -d 'Print the help message.' -f
complete -c undervolt -s v -l verbose -d 'Print debug info' -f
complete -c undervolt -s f -l force   -d 'Allow setting positive offsets' -f
complete -c undervolt -s r -l read    -d 'Read existing values' -f
complete -c undervolt -s t -l temp    -d 'Set temperature target on AC (and battery if --temp-bat is not used)' -x

complete -c undervolt -l temp-bat     -d 'Set temperature target on battery power' -x
complete -c undervolt -l throttlestop -d 'Extract values from ThrottleStop' -f
complete -c undervolt -l tsindex      -d 'ThrottleStop profile index' -x

complete -c undervolt -l core         -d 'core offset (mV)' -x
complete -c undervolt -l gpu          -d 'gpu offset (mV)' -x
complete -c undervolt -l cache        -d 'cache offset (mV)' -x
complete -c undervolt -l uncore       -d 'uncore offset (mV)' -x
complete -c undervolt -l analogio     -d 'analogio offset (mV)' -x

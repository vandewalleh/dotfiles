complete -c smloadr -s h -l help         -d 'Print the help message.' -f
complete -c smloadr -s q -l quality      -d 'The quality of the files to download' -xa 'MP3_128 MP3_320 FLAC'
complete -c smloadr -s p -l path         -d 'The path to download the files to: path with / in the end' -r
complete -c smloadr -s u -l url          -d 'Downloads single deezer url: album/artist/playlist/profile/track url' -x
complete -c smloadr -d d -l downloadmode -d 'Downloads multiple urls from list: "all" for downloadLinks.txt' -f
